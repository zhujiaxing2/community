# SIGs - Special Interest Groups

The full name of SIG is "special interest group". We set up various technical sigs in MindSpore community to
provide an open communication platform for experts, professors and students in this field.
We hope that it can promote technical communication and win-win cooperation, and improve the influence and technical
ability of SIG members through meeting sharing, project development and other activities.
Especially for college students, after joining SIGs, they can participate in MindSpore's development activities
under the guidance of experts, in which they can master the advanced technology in the industry and prepare for future
work. They can also communicate face-to-face with industry experts and professors and meet their own Bole. Up to now,
more than ten sigs have been set up in MindSpore community, including not only the front-end and back-end technologies
of MindSpore framework itself, but also the research of upper algorithms such as AI security and scientific computing.
We also welcome friends with ideas to create their own sigs!

## Current SIGs

| SIG name                                   | Responsibilities                                                                                                                                                  | SIG Leads |
|:-------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------| :-------- |
| [DevelopereXperience](dx/README.md)        | This SIG is responsible for improving the experience of those who upstream contribute or develop applications for MindSpore community.                            | [@jiancao81](https://gitee.com/jiancao81)(cao-jian@cs.sjtu.edu.cn)  |
| [Trusted AI](security/README.md)          | This SIG is responsible for the development of MindSpore security related tools.                                                                                  | [@randywangze](https://gitee.com/randywangze) |
| [Usability](usability/README.md)           | This SIG is responsible for improving the usability of MindSpore for developers.                                                                                  | [@zhangtong](https://gitee.com/tong-zhang) |
| [FrontEnd](frontend/README.md)             | This SIG is responsible for the development of MindSpore front-end expression.                                                                                    | [@wangnan](https://gitee.com/wangnan39) |
| [Compiler](compiler/README.md)             | This SIG is responsible for the development of MindSpore high level graph compilation.                                                                            | [@zh_qh](https://gitee.com/zh_qh) |
| [Executor](executor/README.md)             | This SIG is responsible for the development of MindSpore back-end support for pipeline.                                                                           | [@kisnwang](https://gitee.com/kisnwang) |
| [ModelZoo](modelzoo/README.md)             | This SIG is responsible for the development of MindSpore modelzoo and additional ops.                                                                             | [@chenhaozhe](https://gitee.com/c_34) |
| [Data](data/README.md)                     | This SIG is responsible for the development of MindSpore data processing and data format transformation.                                                          | [@liucunwei](https://gitee.com/liucunwei) |
| [Visualization](visualization/README.md)   | This SIG is responsible for the development of Visualized debugging and optimization.                                                                             | [@liangyongxiong](https://gitee.com/liangyongxiong1024) |
| [MSLITE](mslite/README.md)                 | This SIG is responsible for the development of MindSpore lite.                                                                                                    | [@zhaizhiqiang](https://gitee.com/zhaizhiqiang) |
| [Parallel](parallel/README.md)             | This SIG is responsible for the development of MindSpore's functionality of automatically finding the efficient parallel strategy for DNN training and inference. | [@baiyouhui](https://gitee.com/bert0108) |
| [DataCompliance](datacompliance/README.md) | This SIG aims to reduce the risk of license compliance and help developers to use and share datasets legally.                                                     | [@gopikrishnanrajbahadur](https://gitee.com/gopikrishnanrajbahadur) [@clement_li](https://gitee.com/clement_li) |
| [MindQuantum](mindquantum/README.md)        | This SIG is responsible for the development of MindSpore quantum software and algorithms.             | [@dorothy20212021](https://gitee.com/dorothy20212021)  |

## Resources

You can view the screen recordings of the SIG regular meeting on the official bilibili account of MindSpore.

 [links](https://space.bilibili.com/526894060/channel/seriesdetail?sid=675044)

## Joining a SIG

If you are interested in participating, here are 2 ways to join the above SIGs:

1, Add MindSpore assistant wechat "mindspore0328", and the assistant will invite you to SIG's WeChat group.

2, Pay attention to MindSpore's WeChat public account "MindSpore", we will release the SIG regular meeting information on the public account, and the organizer will publish the WeChat group QR code in the meeting.

## Proposing a new SIG

New SIGs are created when there is sufficient interest in a topic area
and someone volunteers to be the lead for the group and submits a proposal to
the steering committee. The chair facilitates the discussion and helps
synthesize proposals and decisions.

[Propose now!](https://gitee.com/mindspore/community/blob/master/sigs/dx/docs/How%20to%20build%20a%20SIG%20or%20WG_cn.md)

## Create a yaml file for description your SIG

To display the SIG you created on MindSpore's official website, you need to use create_sig_info_template.py to create a yaml file to describe SIG related information. The format of the yaml file can be referred [here](https://gitee.com/openeuler/community/blob/master/sig/README.md).